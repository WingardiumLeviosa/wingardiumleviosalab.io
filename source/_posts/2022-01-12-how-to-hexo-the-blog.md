---
title: 我是如何创建这个博客的
date: 2022-01-12 15:32:04
tags:
banner_img: /img/595860.png
index_img: https://tse1-mm.cn.bing.net/th/id/R-C.7bbe64c2282a997092613d004f0222f2?rik=vAgHvLOc8EDfzQ&riu=http%3a%2f%2ftelcruel.gitee.io%2fmedias%2ffeatureimages%2fhexo.png&ehk=NdmbPZkO%2fgkJrZYV8Mc7nAe1dGbDqERFSjSUfMid5VI%3d&risl=&pid=ImgRaw&r=0
description: 从零开始搭建基于Hexo博客框架+Fluid主题+Markdown文档编写的个人博客网站。
---

# 我是如何创建这个博客的

这个博客使用了[Hexo](https://hexo.io/zh-cn/index.html) 博客框架+[Fluid](https://hexo.fluid-dev.com/)主题+Markdown文档编写。主要过程包括：

1. 安装Hexo博客框架
2. 建立博客文件并部署
3. 下载并更改主题
5. 个性化定制
5. 写作方面

为了防止以后需要重蹈覆辙，本篇文章记录了了博客建立的每一步具体操作，可谓“从零开始使用Hexo搭建博客”。此功略亦可供他人参考。

# Hexo博客框架安装

[Hexo](https://hexo.io/zh-cn/index.html)是一款快速简洁高效的博客框架，可支持本地、远端服务器运行以及使用Github Page功能进行托管，是用于捣鼓自己的笔记和写文章发布的利器。Hexo是基于Node.js的服务，因此首先需要下载[Node.js](http://nodejs.org/), 以及[Git](http://git-scm.com/)（用于后续拉取文件）。之后的过程，可完全根据[Hexo官方建站文档](https://hexo.io/zh-cn/docs/)一步步完成。

{% note primary %}
在使用`hexo`命令时，请使用windows自带的命令行，而不是power shell。
{% endnote %}

# 建立博客文件并部署

安装完毕hexo，此时可以选择一个空文件夹建立博客站点框架，请执行下列命令，Hexo 将会在指定文件夹中新建所需要的文件。

```shell
$ hexo init <folder>
$ cd <folder>
$ npm install
```

{% note info%}
执行后Hexo将会在\<folder\>文件夹建立站点文件。若\<folder\>为空，将在目前文件夹建立站点。
{% endnote %}

此时，指定文件夹将会出现如下文件目录：

```
.
├── _config.yml
├── package.json
├── scaffolds
├── source
|   ├── _drafts
|   └── _posts
└── themes
```

其中，有几个文件极为重要：

- **_config.yml** 该文件为网站配置信息，包括网站标题、作者、时间、语言、主题等重要配置和功能。
- **source/_posts/\.md** source文件夹为博文的资源文件夹，其中的_posts文件夹储存了markdown文件为网站博文。
- **themes** 文件夹储存了第三方主题。

这时我们可以建立一篇空博文，使用如下命令：

```shell
$ hexo new "welcome"
```

此时`/source/_posts`文件夹中建立了 `welcome.md` 文件。接着运行

```shell
$ hexo server
```

此时命令行提示

![image-20220112114410283](https://s2.loli.net/2022/01/12/u8z9aHhtpUs7QOL.png)说明网站已成功在本地部署。浏览器访问 http://localhost:4000/ ，显示网站界面如下：

![image-20220112114528520](https://s2.loli.net/2022/01/12/M9ILzyxhUwDvdCB.png)

此时网站在本地部署成功！

{% note info%}
可在命令行执行Crtl+C 停止网站运行。
{% endnote %}

# 下载并更改主题

默认主题为landscape（如上节末尾的图片）。本博客使用第三方Fluid主题，简洁美观。使用第三方主题的基本方法是先下载对应主题（可在Hexo[主题](https://hexo.io/themes/)网站上找到），将其放入`themes`文件夹，之后在`_config.yml`文件中的`theme`块更改名字为对应主题。

可以在[这里](https://github.com/fluid-dev/hexo-theme-fluid)找到Fluid主题的官方安装教程。

# 个性化定制

可在hexo以及主题各自的的_config.yml文件中更改各项参数，如作者，封面图片等。这些文件基本是自成说明文档的。

# 写作方面

使用markdown的写作规范，包含以下功能：

## 文件头规范

使用--- --- 的模块，如

```markdown
---
title: test
date: 2022-01-11 15:17:25
tags:
index_img: /img/thumb-1920-991454.jpg
banner_img: /img/thumb-1920-991454.jpg
description：
---
```

`index_img`是在博客主页显示的博文缩略图，`banner_img`是显示在文章页上方的大图。图片相对路径是从主目录下的`source`开始算起的。如引用`source`文件夹下的`test.jpg`图片，可直接写为`/test.jpg`。 `description` 是在博客首页显示的文章摘要。

{% note info %}

也可用链接形式直接使用网络图片。

{% endnote %}

## 代码块

可采用` ```<language> ``` ` 风格的代码模式，如使用python代码：

````markdown
```python
def main():
    foo()
    print("python")
```
````

效果如下：

![image-20220112152358702](https://s2.loli.net/2022/01/12/Sf2WwX3YtQuEAb1.png)

{% note info%}
若使用Fluid主题，可以在主题的config文件中开启代码行号显示和一键copy功能。
{% endnote %}

行间代码使用 

````markdown
` <code> `
````

用两个\` 单引号包围起来。 

## 图片

与markdown写作一样，可使用本地或者在线的。

## 注释块

使用bootstrap风格提示块：

```markdown
{% note [class] [no-icon] [summary] %}
Any content (support inline tags too).
{% endnote %}
```


- [class] : Optional parameter. Supported values: default | primary | success | info | warning | danger.
- [no-icon] : Optional parameter. Disable icon in note.

- [summary] : Optional parameter. Optional summary of the note.

{% note info %}
info
{% endnote %}

{% note success %}
success
{% endnote %}

{% note warning %}
warning
{% endnote %}

{% note danger %}
danger
{% endnote %}

## Latex公式块

{% note info %}

使用Fluid主题的Latex公式需要注意公式引擎和渲染器，具体配置过程可以参考[这里](https://hexo.fluid-dev.com/docs/guide/#latex-%E6%95%B0%E5%AD%A6%E5%85%AC%E5%BC%8F)。

{% endnote %}

```markdown
$$
F[f](0) = \frac{1}{T}\int_0^T f(x)dx
$$
```

显示效果：

$$
F[f](0) = \frac{1}{T}\int_0^T f(x)dx
$$
