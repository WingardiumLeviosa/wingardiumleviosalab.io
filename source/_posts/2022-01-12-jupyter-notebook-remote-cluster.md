---
title: 本地浏览器访问在Linux服务器/集群上的Jupyter Notebook
date: 2022-01-12 15:50:50
tags:
index_img: https://tse2-mm.cn.bing.net/th/id/OIP-C.GopePoeSWkBJoayDPdDxDAHaDr?pid=ImgDet&rs=1
banner_img: https://tse1-mm.cn.bing.net/th/id/R-C.62238157e8d9792de116aa2f9bf5b28e?rik=%2f9g%2bVlktP3RoEg&riu=http%3a%2f%2fhostbac.com%2fimg%2fpricing-table%2f01.jpg&ehk=OO2CWmKLLu8J4dpd8beRUhUbWYwlxgWzgPAOA6ktvns%3d&risl=&pid=ImgRaw&r=0
description: 使用远程服务器/集群开启jupyter服务，通过ssl在本地终端连接并通过浏览器访问。
---

# 本地浏览器访问在Linux服务器/集群上的Jupyter Notebook

使用远程服务器/集群开启jupyter服务，通过ssl在本地终端连接并通过浏览器访问。

# 安装必要软件

在服务器端安装python3，Anaconda，ipython，jupyter notebook

# 生成密钥

1. 服务器输入```ipython``` 命令进入交互式python程序
2. 执行以下命令，生成密钥：

```python
from notebook.auth import passwd  
passwd()
```

3. 期间会要求建立密码，该密码为后续远程登陆jupyter notebook使用。最后输出的样子：

```shell
In [1]: from notebook.auth import passwd
In [2]: passwd()
Enter password:
Verify password:
Out[2]: 'argon2:$argon2id$v=19$m=10240,t=10,p=8$RDYoZluYAGXX6xfMqOQtsA$uFuTIISG07y9oXXdTmMNF92AG4v760XZzd+Dpy2M550'
```

'argon2:xxxxxx' 即为密钥，提前复制，后续会使用到。

# SSL 加密配置

由于很多服务器/集群 无法直接使用ip:port 访问jupyter服务，需要通过SSL转发。配置SSL需要用到公钥和密钥。直接在```/home/username/.jupyter/``` 文件夹中执行

```shell
$ openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout mycert.pem -out mycert.pem
```

# 配置jupyter服务文件

1. 在服务器运行 ```jupyter notebook --generate-config``` 初始化notebook配置，此时在 ``` /home/username/.jupyter/``` 文件夹中新建了```jupyter_notebook_config.py```文件
2. 直接在文件开头加入：

```python
c.NotebookApp.password=u'argon2:xxxxxx'  # 第二步中生成的长密钥                     
c.NotebookApp.certfile = u'/home/username/.jupyter/mycert.pem'  # 上一步生成的密钥位置
c.NotebookApp.keyfile = u'/home/username/.jupyter/mykey.key'               
c.NotebookApp.ip = 'localhost'
c.NotebookApp.open_browser = False  # 不打开浏览器
c.NotebookApp.port = 8889     # jupyter服务端口，可自定义
c.NotebookApp.allow_origin = '*'
```

此样例中jupyter在服务器的访问端口为8889

# 启动jupyter notebook

1. 直接启动jupyter notebook ```jupyter notebook``` 。如遇notebook命令无法找到问题可以尝试先``` export PATH=$PATH:$HOME/.local/bin```  再启动notebook。此时命令行大致为如下输出，表明服务已启动。

![](https://s2.loli.net/2022/01/12/katRY8ABx4QHEgq.png)



```shell
$ [I 22:20:12.455 NotebookApp] Serving notebooks from local directory: /home/chaow
$ [I 22:20:12.455 NotebookApp] Jupyter Notebook 6.4.6 is running at:
$ [I 22:20:12.455 NotebookApp] https://localhost:8889/
$ [I 22:20:12.455 NotebookApp] Use Control-C to stop this server and shut down all kernels (twice to skip confirmation).
```

2. 这时在本地直接使用https://localhost:8889 或者http:// 远程ip:8889 都是无法打开notebook的。我们需要进行最后一步的ssl设置。在本地cmd或者power shell 中输入：

```shell
 $ ssh -N -f -L localhost:8787:localhost:8889 -p 9135 username@远程ip
```

其中8787是之后访问jupyter的本地端口（可更改为其他），8889是jupyter在服务器上的端口（在jupyter配置文件中），9135是ssh访问的端口（通常为22，我这边是9135），username是登录远程服务器的用户名，远程ip是服务器ip。之后会提示输入密码，输入后命令行呈现假死状态，不用管。

3. 在本地浏览器输入https://localhost:8787 （8787就是上一步设置的本地端口），提示链接不安全，选择继续访问即可。此时应该成功连接到远程jupyter！
4. 此时可以关闭刚才假死的命令行，之后的访问只需要在服务器上开启jupyter即可。

![](https://s2.loli.net/2022/01/12/Uuq4D2fAIYXt9Vw.png)

